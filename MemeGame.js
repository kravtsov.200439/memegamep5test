function preload() {
  img = loadImage('data/memes/ancap.jpg');
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  scene1 = new Scene(img, "red", "ancap", "pepe", "npc");
}

function draw() {
  background(180);
  scene1.update();
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
