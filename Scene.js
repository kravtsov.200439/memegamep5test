class scene {
  constructor(Image, BGColour, CorOpt, IncorOpt1, IncorOpt2) {
    this.img = Image;
    this.bgcolour = BGColour;
    this.coropt = CorOpt;
    this.incoropt1 = IncorOpt1;
    this.incoropt2 = IncorOpt2;
  }

  update() {
    image(this.img, windowWidth/2, windowHeight/2);
  }
}
